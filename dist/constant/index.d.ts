export declare const ERROR_MESSAGE: {
    WRITE_ACCESS_EXCEED: string;
    READ_ACCESS_EXCEED: string;
    SHORT_URL_NOT_FOUND: string;
};
