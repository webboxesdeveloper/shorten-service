"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERROR_MESSAGE = void 0;
exports.ERROR_MESSAGE = {
    WRITE_ACCESS_EXCEED: 'Write access exceeds',
    READ_ACCESS_EXCEED: 'Read access exceeds',
    SHORT_URL_NOT_FOUND: 'Short url does not exist',
};
//# sourceMappingURL=index.js.map