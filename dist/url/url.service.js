"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UrlService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const url_map_entity_1 = require("./entities/url-map.entity");
const typeorm_2 = require("typeorm");
const config_1 = require("../config");
const read_access_log_entity_1 = require("./entities/read-access-log.entity");
const write_access_log_entity_1 = require("./entities/write-access-log.entity");
const constant_1 = require("../constant");
let UrlService = class UrlService {
    constructor(urlMapRepository, readAccessLogRepository, writeAccessLogRepository) {
        this.urlMapRepository = urlMapRepository;
        this.readAccessLogRepository = readAccessLogRepository;
        this.writeAccessLogRepository = writeAccessLogRepository;
    }
    async shortenUrl(createShortUrlDto) {
        const numberOfLatestLogs = await typeorm_2.getRepository(write_access_log_entity_1.WriteAccessLogEntity)
            .createQueryBuilder('writeAccessLog')
            .where(`writeAccessLog.createdAt BETWEEN DATE_SUB(NOW(), interval ${config_1.config.accessLimit.duration} second) AND NOW()`)
            .getCount();
        if (numberOfLatestLogs >= config_1.config.accessLimit.write) {
            throw new common_1.HttpException(constant_1.ERROR_MESSAGE.WRITE_ACCESS_EXCEED, common_1.HttpStatus.FORBIDDEN);
        }
        const numberOfUniqueUrls = config_1.config.numberOfUniqueUrls;
        const totalNumberOfShortened = await this.urlMapRepository.count();
        let shortenedUri = "1";
        if (totalNumberOfShortened >= numberOfUniqueUrls) {
            const query = typeorm_2.getRepository(url_map_entity_1.UrlMapEntity)
                .createQueryBuilder('urlMap')
                .take(1)
                .addOrderBy('urlMap.updatedAt', 'ASC');
            const oldestEntity = await query.getOne();
            shortenedUri = oldestEntity.shortenedUri;
            await this.urlMapRepository.delete(oldestEntity.id);
        }
        else {
            shortenedUri = (totalNumberOfShortened + 1).toString();
        }
        const entity = Object.assign(new url_map_entity_1.UrlMapEntity(), {
            originalUrl: createShortUrlDto.url,
            shortenedUri,
        });
        const log = Object.assign(new write_access_log_entity_1.WriteAccessLogEntity(), {
            originalUrl: createShortUrlDto.url,
        });
        await this.writeAccessLogRepository.save(log);
        return await this.urlMapRepository.save(entity);
    }
    async navigateToShortUrl(id) {
        const numberOfLatestLogs = await typeorm_2.getRepository(read_access_log_entity_1.ReadAccessLogEntity)
            .createQueryBuilder('readAccessLog')
            .where(`readAccessLog.createdAt BETWEEN DATE_SUB(NOW(), interval ${config_1.config.accessLimit.duration} second) AND NOW()`)
            .getCount();
        if (numberOfLatestLogs >= config_1.config.accessLimit.read) {
            throw new common_1.HttpException(constant_1.ERROR_MESSAGE.READ_ACCESS_EXCEED, common_1.HttpStatus.FORBIDDEN);
        }
        const entity = await this.urlMapRepository.findOne({
            where: {
                shortenedUri: id
            }
        });
        if (!entity) {
            throw new common_1.HttpException(constant_1.ERROR_MESSAGE.SHORT_URL_NOT_FOUND, common_1.HttpStatus.BAD_REQUEST);
        }
        const log = Object.assign(new read_access_log_entity_1.ReadAccessLogEntity(), {
            shortenedUri: id
        });
        await this.readAccessLogRepository.save(log);
        return entity.originalUrl;
    }
};
UrlService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(url_map_entity_1.UrlMapEntity)),
    __param(1, typeorm_1.InjectRepository(read_access_log_entity_1.ReadAccessLogEntity)),
    __param(2, typeorm_1.InjectRepository(write_access_log_entity_1.WriteAccessLogEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], UrlService);
exports.UrlService = UrlService;
//# sourceMappingURL=url.service.js.map