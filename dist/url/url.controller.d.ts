import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { UrlService } from './url.service';
export declare class UrlController {
    private readonly urlService;
    constructor(urlService: UrlService);
    shortenUrl(createShortUrlDto: CreateShortUrlDto, res: any): Promise<any>;
    navigateToShortUrl(id: string, res: any): Promise<void>;
}
