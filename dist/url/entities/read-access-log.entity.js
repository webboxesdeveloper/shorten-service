"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReadAccessLogEntity = void 0;
const typeorm_1 = require("typeorm");
let ReadAccessLogEntity = class ReadAccessLogEntity {
    beforeCreate() {
        this.createdAt = (new Date()).toISOString();
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ReadAccessLogEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        type: 'text',
        nullable: false,
    }),
    __metadata("design:type", String)
], ReadAccessLogEntity.prototype, "shortenedUri", void 0);
__decorate([
    typeorm_1.Column({
        type: 'datetime',
        nullable: true
    }),
    __metadata("design:type", String)
], ReadAccessLogEntity.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ReadAccessLogEntity.prototype, "beforeCreate", null);
ReadAccessLogEntity = __decorate([
    typeorm_1.Entity('read_access_log')
], ReadAccessLogEntity);
exports.ReadAccessLogEntity = ReadAccessLogEntity;
//# sourceMappingURL=read-access-log.entity.js.map