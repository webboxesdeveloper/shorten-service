export declare class ReadAccessLogEntity {
    id: number;
    shortenedUri: string;
    createdAt: string;
    beforeCreate(): void;
}
