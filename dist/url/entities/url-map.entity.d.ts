export declare class UrlMapEntity {
    id: number;
    originalUrl: string;
    shortenedUri: string;
    createdAt: string;
    updatedAt: string;
    beforeCreate(): void;
    beforeUpdate(): void;
}
