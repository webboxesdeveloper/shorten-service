export declare class WriteAccessLogEntity {
    id: number;
    originalUrl: string;
    createdAt: string;
    beforeCreate(): void;
}
