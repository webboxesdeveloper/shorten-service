import { UrlMapEntity } from './entities/url-map.entity';
import { Repository } from 'typeorm';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { ReadAccessLogEntity } from './entities/read-access-log.entity';
import { WriteAccessLogEntity } from './entities/write-access-log.entity';
export declare class UrlService {
    private readonly urlMapRepository;
    private readonly readAccessLogRepository;
    private readonly writeAccessLogRepository;
    constructor(urlMapRepository: Repository<UrlMapEntity>, readAccessLogRepository: Repository<ReadAccessLogEntity>, writeAccessLogRepository: Repository<WriteAccessLogEntity>);
    shortenUrl(createShortUrlDto: CreateShortUrlDto): Promise<any>;
    navigateToShortUrl(id: any): Promise<any>;
}
