export declare const config: {
    limit: string;
    numberOfUniqueUrls: number;
    accessLimit: {
        write: number;
        read: number;
        duration: number;
    };
};
