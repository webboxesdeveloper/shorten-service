"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
exports.config = {
    limit: '100mb',
    numberOfUniqueUrls: 1000000,
    accessLimit: {
        write: 100,
        read: 1000,
        duration: 60,
    }
};
//# sourceMappingURL=index.js.map