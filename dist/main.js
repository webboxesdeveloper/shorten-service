"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const common_1 = require("@nestjs/common");
const app_module_1 = require("./app.module");
const bodyParser = require("body-parser");
const config_1 = require("./config");
const swagger_1 = require("@nestjs/swagger");
const path_1 = require("path");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.useGlobalPipes(new common_1.ValidationPipe({
        validationError: { target: false, value: false },
    }));
    app.use(bodyParser.json({ limit: config_1.config.limit }));
    app.use(bodyParser.urlencoded({ limit: config_1.config.limit, extended: false }));
    app.enableCors();
    app.useStaticAssets(path_1.join(__dirname, '..', 'views/default'));
    app.setBaseViewsDir(path_1.join(__dirname, '..', 'views'));
    const options = new swagger_1.DocumentBuilder()
        .setTitle('Url Shorten Service')
        .setDescription('')
        .setVersion('1.0')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    swagger_1.SwaggerModule.setup('docs', app, document);
    await app.listen(4444);
}
bootstrap();
//# sourceMappingURL=main.js.map