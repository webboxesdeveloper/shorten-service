import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';
import { config } from './config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    validationError: { target: false, value: false },
  }));
  app.use(bodyParser.json({ limit: config.limit }));
  app.use(bodyParser.urlencoded({ limit: config.limit, extended: false }));
  app.enableCors();

  app.useStaticAssets(join(__dirname, '..', 'views/default'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));

  const options = new DocumentBuilder()
    .setTitle('Url Shorten Service')
    .setDescription('')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  await app.listen(4444);
}
bootstrap();
