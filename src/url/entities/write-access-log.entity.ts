import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('write_access_log')
export class WriteAccessLogEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    nullable: false,
  })
  originalUrl: string;

  @Column({
    type: 'datetime',
    nullable: true
  })
  createdAt: string;

  @BeforeInsert()
  beforeCreate() {
    this.createdAt = (new Date()).toISOString();
  }

}
