import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('read_access_log')
export class ReadAccessLogEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    nullable: false,
  })
  shortenedUri: string;

  @Column({
    type: 'datetime',
    nullable: true
  })
  createdAt: string;

  @BeforeInsert()
  beforeCreate() {
    this.createdAt = (new Date()).toISOString();
  }

}
