import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('url_map')
export class UrlMapEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    nullable: false,
  })
  originalUrl: string;

  @Column({
    type: 'text',
    nullable: false,
  })
  shortenedUri: string;

  @Column({
    type: 'datetime',
    nullable: true
  })
  createdAt: string;

  @Column({
    type: 'datetime',
    nullable: true
  })
  updatedAt: string;

  @BeforeInsert()
  beforeCreate() {
    this.createdAt = new Date().toISOString();
    this.updatedAt = new Date().toISOString();
  }

  @BeforeUpdate()
  beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

}
