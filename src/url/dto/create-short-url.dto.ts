import { IsNotEmpty, IsString, IsUrl } from 'class-validator';
import { ApiProperty } from "@nestjs/swagger";

export class CreateShortUrlDto {

  @ApiProperty()
  @IsString()
  @IsUrl()
  @IsNotEmpty()
  readonly url: string;
}
