import { Module } from '@nestjs/common';
import { UrlController } from './url.controller';
import { UrlService } from './url.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UrlMapEntity } from './entities/url-map.entity';
import { ReadAccessLogEntity } from './entities/read-access-log.entity';
import { WriteAccessLogEntity } from './entities/write-access-log.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UrlMapEntity,
      ReadAccessLogEntity,
      WriteAccessLogEntity,
    ])
  ],
  controllers: [UrlController],
  providers: [UrlService]
})
export class UrlModule {}
