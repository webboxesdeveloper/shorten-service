import { Body, Controller, Get, HttpStatus, Param, Post, Res, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { UrlService } from './url.service';

@Controller('url')
export class UrlController {
  constructor(
    private readonly urlService: UrlService,
  ) {
  }

  @ApiTags('url')
  @ApiOperation({
    description: 'generate short url',
  })
  @Post('shorten')
  async shortenUrl(@Body() createShortUrlDto: CreateShortUrlDto, @Res() res: any) {
    const response = await this.urlService.shortenUrl(createShortUrlDto);

    if (response) {
      return res.status(HttpStatus.OK).json({
        statusCode: 200,
        response,
      });
    }
  }

  @ApiTags('url')
  @ApiOperation({
    description: 'navigate to short url',
  })
  @Get(':id')
  async navigateToShortUrl(@Param('id') id: string, @Res() res: any) {
    const response = await this.urlService.navigateToShortUrl(id);

    if (response) {
      res.redirect(response);
    }
  }
}
