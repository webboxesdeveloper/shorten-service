import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UrlMapEntity } from './entities/url-map.entity';
import { getRepository, Repository } from 'typeorm';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import {config} from '../config';
import { ReadAccessLogEntity } from './entities/read-access-log.entity';
import { WriteAccessLogEntity } from './entities/write-access-log.entity';
import { ERROR_MESSAGE } from '../constant';

@Injectable()
export class UrlService {
  constructor(
    @InjectRepository(UrlMapEntity) private readonly urlMapRepository: Repository<UrlMapEntity>,
    @InjectRepository(ReadAccessLogEntity) private readonly readAccessLogRepository: Repository<ReadAccessLogEntity>,
    @InjectRepository(WriteAccessLogEntity) private readonly writeAccessLogRepository: Repository<WriteAccessLogEntity>,
  ) {
  }

  async shortenUrl(createShortUrlDto: CreateShortUrlDto): Promise<any> {
    const numberOfLatestLogs = await getRepository(WriteAccessLogEntity)
      .createQueryBuilder('writeAccessLog')
      .where(`writeAccessLog.createdAt BETWEEN DATE_SUB(NOW(), interval ${config.accessLimit.duration} second) AND NOW()`)
      .getCount();

    if (numberOfLatestLogs >= config.accessLimit.write) {
      throw new HttpException(ERROR_MESSAGE.WRITE_ACCESS_EXCEED, HttpStatus.FORBIDDEN);
    }

    const numberOfUniqueUrls = config.numberOfUniqueUrls;
    const totalNumberOfShortened = await this.urlMapRepository.count();
    let shortenedUri = "1";

    if (totalNumberOfShortened >= numberOfUniqueUrls) {
      const query = getRepository(UrlMapEntity)
        .createQueryBuilder('urlMap')
        .take(1)
        .addOrderBy('urlMap.updatedAt', 'ASC');

      const oldestEntity = await query.getOne();
      shortenedUri = oldestEntity.shortenedUri;
      await this.urlMapRepository.delete(oldestEntity.id);
    } else {
      shortenedUri = (totalNumberOfShortened + 1).toString();
    }

    const entity = Object.assign(new UrlMapEntity(), {
      originalUrl: createShortUrlDto.url,
      shortenedUri,
    });

    const log = Object.assign(new WriteAccessLogEntity(), {
      originalUrl: createShortUrlDto.url,
    });

    await this.writeAccessLogRepository.save(log);

    return await this.urlMapRepository.save(entity);
  }

  async navigateToShortUrl(id): Promise<any> {
    const numberOfLatestLogs = await getRepository(ReadAccessLogEntity)
      .createQueryBuilder('readAccessLog')
      .where(`readAccessLog.createdAt BETWEEN DATE_SUB(NOW(), interval ${config.accessLimit.duration} second) AND NOW()`)
      .getCount();

    if (numberOfLatestLogs >= config.accessLimit.read) {
      throw new HttpException(ERROR_MESSAGE.READ_ACCESS_EXCEED, HttpStatus.FORBIDDEN);
    }

    const entity = await this.urlMapRepository.findOne({
      where: {
        shortenedUri: id
      }
    });

    if (!entity) {
      throw new HttpException(ERROR_MESSAGE.SHORT_URL_NOT_FOUND, HttpStatus.BAD_REQUEST);
    }

    const log = Object.assign(new ReadAccessLogEntity(), {
      shortenedUri: id
    });

    await this.readAccessLogRepository.save(log);

    return entity.originalUrl;
  }
}
