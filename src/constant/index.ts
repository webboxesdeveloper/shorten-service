export const ERROR_MESSAGE = {
  WRITE_ACCESS_EXCEED: 'Write access exceeds',
  READ_ACCESS_EXCEED: 'Read access exceeds',
  SHORT_URL_NOT_FOUND: 'Short url does not exist',
};