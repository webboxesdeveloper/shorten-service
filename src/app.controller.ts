import { Controller, Get, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiExcludeEndpoint } from '@nestjs/swagger';
import {join} from 'path';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiExcludeEndpoint()
  @Get('/')
  renderViews(@Res() res: any) {
    res.sendFile(join(process.cwd(), 'views/default/index.html'));
  }
}
