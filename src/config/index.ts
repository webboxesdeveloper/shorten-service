export const config = {
  limit: '100mb',
  numberOfUniqueUrls: 1000000,
  accessLimit: {
    write: 100, // limited url submissions
    read: 1000, // limited short url accesses
    duration: 60, // means 60 seconds
  }
};
